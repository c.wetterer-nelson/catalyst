# Windows-specific builder configurations and build commands

## Base configurations

.windows:
    variables:
        GIT_SUBMODULE_STRATEGY: none
        GIT_CLONE_PATH: "$CI_BUILDS_DIR\\catalyst-ci-ext\\$CI_CONCURRENT_ID"

### Build and test

.windows_build:
    extends: .windows

    variables:
        # Note that shell runners only support runners with a single
        # concurrency level. We can't use `$CI_CONCURRENCY_ID` because this may
        # change between the build and test stages which CMake doesn't support.
        # Even if we could, it could change if other runners on the machine
        # could run at the same time, so we drop it.
        GIT_CLONE_PATH: "$CI_BUILDS_DIR\\catalyst-ci"

.windows_vs2022:
    extends: .windows_build

    variables:
        VCVARSALL: "${VS170COMNTOOLS}\\..\\..\\VC\\Auxiliary\\Build\\vcvarsall.bat"
        VCVARSPLATFORM: "x64"
        VCVARSVERSION: "14.32.31326"

.windows_vs2022_mpi_replay:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_mpi_replay

.windows_vs2022_replay:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_replay

.windows_vs2022_replay_wrappings:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_replay_wrappings
        CATALYST_SETUP_PYTHON_ENV: 1
        CMAKE_BUILD_TYPE: "Release"

.windows_vs2022_mpi_replay_wrappings:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_mpi_replay_wrappings
        CATALYST_SETUP_PYTHON_ENV: 1
        CMAKE_BUILD_TYPE: "Release"

## Tags

.windows_builder_tags:
    tags:
        - msvc-19.32
        - concurrent
        - shell
        - vs2022
        - windows-x86_64
        - paraview

.windows_mpi_builder_tags:
    tags:
        - msvc-19.32
        - concurrent
        - shell
        - vs2022
        - windows-x86_64
        - msmpi
        - paraview

## Windows-specific scripts

.before_script_windows: &before_script_windows
    - Invoke-Expression -Command .gitlab/ci/cmake.ps1
    - Invoke-Expression -Command .gitlab/ci/ninja.ps1
    - $pwdpath = $pwd.Path
    - Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab;$pwdpath\.gitlab\cmake\bin;$env:PATH"
    - cmake --version
    - ninja --version
    - Invoke-Expression -Command .gitlab/ci/gtest.ps1
    - Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab\gtest\bin;$env:PATH"
    # Setup python
    - if ( "$env:CATALYST_SETUP_PYTHON_ENV" -eq "1" ) { cmake -P .gitlab/ci/download_python.cmake }
    - if ( "$env:CATALYST_SETUP_PYTHON_ENV" -eq "1" ) { Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab\python;$env:PATH" }
    - if ( "$env:CATALYST_SETUP_PYTHON_ENV" -eq "1" ) { Set-Item -Force -Path "env:PYTHONHOME" -Value "$pwdpath\.gitlab\python" }
    # use cmd.exe so we can use "&&" and catch any errors
    - if ( "$env:CATALYST_SETUP_PYTHON_ENV" -eq "1" ) { cmd /c "python -m venv venv && .\venv\Scripts\activate.bat && pip install numpy mpi4py" }
    # python env was activated only for the lifetime of cmd. Activate for powershell for the rest of the script
    - if ( "$env:CATALYST_SETUP_PYTHON_ENV" -eq "1" ) { . .\venv\Scripts\Activate.ps1 }

.cmake_build_test_windows:
    stage: build-test

    script:
        - *before_script_windows
        - Invoke-Expression -Command .gitlab/ci/vcvarsall.ps1
        - ctest -VV -S .gitlab/ci/ctest_configure.cmake
        - ctest -VV -S .gitlab/ci/ctest_build.cmake | Out-File -FilePath compile_output.log
        - ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake
    interruptible: true
    timeout: 5 minutes
