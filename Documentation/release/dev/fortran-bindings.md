## Fortran bindings

The Catalyst API is wrapped in Fortran. Simulation developers can
now access Catalyst in Fortran. To compile the Fortran interface
use `CATALYST_WRAP_FORTRAN=ON` when compiling Catalyst.
